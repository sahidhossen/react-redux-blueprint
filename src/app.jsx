import React from 'react';
import { Provider } from 'react-redux';
import '../styles/index.scss';
import Home from './components/Home/Home';
import store from './store'


export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <Home/>
        </Provider>
    )
  }
}
