import React from 'react';
import { connect } from 'react-redux';
import { fetchUsers } from  '../../actions/userActions';

@connect( (store) => {
    return {
        user : store.user.user,
    }
})
export default class Home extends React.Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
        this.props.dispatch( fetchUsers() )
    }

    render(){
        console.log(this.props.user);
        return (
            <div className="excel-container">
                <h1> This is home page </h1>
            </div>
        )
    }
}